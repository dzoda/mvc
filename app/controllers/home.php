<?php
/**
 * Created by PhpStorm.
 * User: gallkster
 * Date: 25.10.17.
 * Time: 15.35
 */
class Home extends Controller {

  public function index() {
    $user = $this->model('User');
    $user->name = 'Dzodihno';
    $this->view('index', []);
    echo $user->name;
  }

  public function test() {
    $user = $this->model('User');
    $user->name = 'Dzodarius';
    $data = ['name' => $user->name];
    $this->view('index',$data );
  }

}