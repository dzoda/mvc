<?php
/**
 * Created by PhpStorm.
 * User: gallkster
 * Date: 25.10.17.
 * Time: 15.27
 */

class Controller {

  public function model($model) {
    require_once '../app/models/' . $model . '.php';

    return new $model();
  }

  public function view($view, $data) {

    require_once '../app/views/' . $view . '.php';

  }
}